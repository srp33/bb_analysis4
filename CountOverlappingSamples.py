import sys, glob

filePattern = sys.argv[1]

sampleIDs = None
filePaths = glob.glob(filePattern)

for filePath in filePaths:
    with open(filePath) as theFile:
        theFile.readline()

        fileSampleIDs = set()
        for line in theFile:
            fileSampleIDs.add(line.rstrip("\n").split("\t")[0])

        if sampleIDs == None:
            sampleIDs = fileSampleIDs
        else:
            sampleIDs = sampleIDs & fileSampleIDs

print("{} common samples across {} files for {}.".format(len(sampleIDs), len(filePaths), filePattern))
