import glob
import sys

algos = glob.glob("AlgorithmScripts/Classification/*/*/*/*")
algos_remove = glob.glob("AlgorithmScripts/Classification/tsv/mlr/multinom/*")
algos_remove = algos_remove + glob.glob("AlgorithmScripts/Classification/arff/weka/MultilayerPerceptron/*")
algos_remove = algos_remove + glob.glob("AlgorithmScripts/Classification/tsv/mlr/boosting/*")
algos_remove = algos_remove + glob.glob("AlgorithmScripts/Classification/tsv/mlr/rda/*")

for a in algos_remove:
  algos.remove(a)

output_file = 'AllClassificationAlgorithms.list'
out = open(output_file,'w')
for a in algos:
  out.write(a + '\n')
out.close()

algos_def = glob.glob("AlgorithmScripts/Classification/*/*/*/default*")
algos_remove = glob.glob("AlgorithmScripts/Classification/tsv/mlr/multinom/default*")
algos_remove = algos_remove + glob.glob("AlgorithmScripts/Classification/arff/weka/MultilayerPerceptron/default*")
algos_remove = algos_remove + glob.glob("AlgorithmScripts/Classification/tsv/mlr/boosting/default*")
algos_remove = algos_remove + glob.glob("AlgorithmScripts/Classification/tsv/mlr/rda/default*")

for a in algos_remove:
  algos_def.remove(a)

output_file = 'AllClassificationAlgorithmsDefault.list'
out = open(output_file,'w')
for a in algos_def:
  out.write(a + '\n')
out.close()
