import sys, gzip

inFilePath1 = sys.argv[1]
inFilePath2 = sys.argv[2]
indices = [int(x) for x in sys.argv[3].split(",")]

data1 = set()
with gzip.open(inFilePath1) as inFile1:
    for line in inFile1:
        lineItems = line.decode().rstrip("\n").split("\t")
        lineItems = tuple([lineItems[i] for i in indices])

        data1.add(lineItems)

data2 = set()
with gzip.open(inFilePath2) as inFile2:
    for line in inFile2:
        lineItems = line.decode().rstrip("\n").split("\t")
        lineItems = tuple([lineItems[i] for i in indices])

        data2.add(lineItems)

dataDiff = data1 - data2

#print(len(data1))
#print(len(data2))
#print(len(dataDiff))

#print("Differences:")

outLines = []

for x in sorted(list(dataDiff)):
    x = list(x)

    if x[4].startswith("mlr") or x[4].startswith("sklearn"):
        x[4] = "tsv__" + x[4]
    else:
        x[4] = "arff__" + x[4]
    x[4] = x[4].replace("/", "__")

    outLines.append("rm -rfv Analysis4/{}/{}/iteration{}/{}".format(x[0], x[1], x[2], x[4]))

outLines = sorted(list(set(outLines)))

for line in outLines:
    print(line)
