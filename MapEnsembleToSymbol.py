import sys, gzip

inFilePath = sys.argv[1]
mapFilePath = sys.argv[2]
outFilePath = sys.argv[3]

print("Reading {}".format(mapFilePath))
mapDict = {}
genesToRemove = set()
with gzip.open(mapFilePath) as mapFile:
    mapFile.readline()
    for line in mapFile:
        lineItems = line.decode().rstrip("\n").split("\t")
        ensemblID = lineItems[0]
        geneSymbol = lineItems[1]

        if geneSymbol == "":
            continue

        # This means we have more than one Ensembl ID for this gene symbol,
        # so we don't want to convert the Ensembl ID to the gene symbol.
        if geneSymbol in mapDict.values():
            genesToRemove.add(ensemblID)

        mapDict[ensemblID] = geneSymbol

for ensemblID in genesToRemove:
    del mapDict[ensemblID]

print("Parsing {}".format(inFilePath))
lineCount = 0
with gzip.open(inFilePath) as inFile:
    headerItems = inFile.readline().decode().rstrip("\n").split("\t")
    geneIndex = headerItems.index("Gene")

    with gzip.open(outFilePath, 'w') as outFile:
        outFile.write(("\t".join(headerItems) + "\n").encode())

        for line in inFile:
            lineCount += 1
            if lineCount % 10000 == 0:
                print(lineCount)

            lineItems = line.decode().rstrip("\n").split("\t")
            if lineItems[geneIndex] in mapDict:
                lineItems[geneIndex] = mapDict[lineItems[geneIndex]]

            outLine = "\t".join(lineItems) + "\n"
            outFile.write(outLine.encode())
