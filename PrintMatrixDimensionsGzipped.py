import os, sys, gzip

inFilePath = sys.argv[1]

inFile = gzip.open(inFilePath)

allNumCols = set()

numRows = 0
for line in inFile:
    allNumCols.add(len(line.rstrip().split("\t")))
    numRows += 1
inFile.close()

print "Number Rows: %i" % numRows

if len(allNumCols) > 1:
    print "The number of columns differed for some of the rows: %s" % ",".join([str(x) for x in sorted(list(allNumCols))])
else:
    print "Number Columns: %i" % list(allNumCols)[0]
