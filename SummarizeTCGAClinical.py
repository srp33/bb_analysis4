import sys, gzip

clinicalFilePath = sys.argv[1]
exprFilePath = sys.argv[2]
#outFilePath = sys.argv[3]

exprFile = gzip.open(exprFilePath)
exprFile.readline()
exprSamples = set([line.rstrip("\n").split("\t")[0] for line in exprFile])
exprFile.close()

clinicalDict = {}
clinicalFile = gzip.open(clinicalFilePath)
clinHeaderItems = clinicalFile.readline().rstrip("\n").split("\t")

cdeDict = {}

for line in clinicalFile:
    lineItems = line.rstrip("\n").split("\t")
    variableName = lineItems[0]
    cde = lineItems[2]
    cdeDict[variableName] = cde

    for i in range(3, len(clinHeaderItems)):
        sampleID = clinHeaderItems[i][:12]
        value = lineItems[i]

        if sampleID not in exprSamples:
            continue

        if variableName not in clinicalDict:
            clinicalDict[variableName] = {}

        clinicalDict[variableName][sampleID] = value

clinicalFile.close()

selectClinicalDict = {}
missingTerms = set(["NA", "[Not Available]", "[Unknown]", "[Not Applicable]"])
for variableName, sampleDict in clinicalDict.items():
    nonMissing = [x for x in sampleDict.values() if x not in missingTerms]
    proportionNonMissing = float(len(nonMissing)) / float(len(exprSamples))
    numUnique = len(set(nonMissing))

    #if proportionNonMissing > 0.75 and numUnique > 1:
    if proportionNonMissing > 0.05:
        print(variableName)
        print(cdeDict[variableName])
        print(proportionNonMissing)
        print("Values:")
        #for x in sorted(list(set(nonMissing)))[:min(10, len(set(nonMissing)))]:
        for x in sorted(list(set(nonMissing))):
            print("  " + x + ": " + str(nonMissing.count(x)))
        print("  Total: " + str(len(nonMissing)))
        print("")
