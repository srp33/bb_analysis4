import sys, gzip

inFilePath = sys.argv[1]
#DatasetID	Class	Iteration	ClassifAlgoGroup	Metric	Value	ClassificationAlgorithm
#GSE10320	Prognostic__Relapse	1	weka/Bagging	AUROC	0.651433691756272	weka/Bagging/alt__bagSizePercent=100_classifier=weka.classifiers.functions.SimpleLogistic_numIterations=100_representCopiesUsingWeights=

uniqueDatasets = set()
uniqueIterations = set()
uniqueAlgos = set()
actualCombos = set()

with gzip.open(inFilePath) as inFile:
    inFile.readline()

    for line in inFile:
        line = line.decode().rstrip("\n")
        lineItems = line.split("\t")
        dataset = lineItems[0] + "__" + lineItems[1]
        iteration = lineItems[2]
        algo = lineItems[4]

        uniqueDatasets.add(dataset)
        uniqueIterations.add(iteration)
        uniqueAlgos.add(algo)
        actualCombos.add((dataset, iteration, algo))

possibleCombos = set()
for dataset in uniqueDatasets:
    for iteration in uniqueIterations:
        for algo in uniqueAlgos:
            possibleCombos.add((dataset, iteration, algo))

diffCombos = possibleCombos - actualCombos

print(len(possibleCombos))
print(len(actualCombos))
print(len(diffCombos))

for combo in sorted(list(diffCombos)):
    print(combo)
