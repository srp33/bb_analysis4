import sys, os

colIndex = int(sys.argv[1])

print(sys.stdin.readline().rstrip("\n"))

for line in sys.stdin:
    lineItems = line.rstrip("\n").split("\t")
    algorithm = lineItems[colIndex]
    #Analysis5/GSE10320/Prognostic__Relapse/iteration1/arff__weka__Correlation__default/arff__weka__Bagging__default__bagSizePercent=75_classifier=weka.classifiers.trees.REPTree_numIterations=10_representCopiesUsingWeights=/Metrics.tsv

    algorithm = os.path.basename(os.path.dirname(os.path.dirname(algorithm)))
    algItems = algorithm.split("__")
    #algorithm = "{}/{}/{}".format(algItems[1], algItems[2], "__".join(algItems[3:]))
    algorithm = "{}/{}".format(algItems[1], algItems[2])

    lineItems[colIndex] = algorithm
    print("\t".join(lineItems))
