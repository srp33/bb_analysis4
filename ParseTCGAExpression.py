import sys, gzip, math

exprFilePath = sys.argv[1]
cancerTypesFilePath = sys.argv[2]
cancerType = sys.argv[3]
outFilePath = sys.argv[4]

def calcAverageAndLogTransform(values):
    if len(values) == 0:
        print("No data found.")
        sys.exit(1)
    else:
        values = [float(x) for x in values]
        return math.log((sum(values) / len(values)) + 1, 2)

print("Parsing " + exprFilePath + " for " + cancerType)

sampleIDs = set()
cancerTypesFile = gzip.open(cancerTypesFilePath)
for line in cancerTypesFile:
    lineItems = line.rstrip("\n").split("\t")
    if lineItems[1] == cancerType:
        sampleIDs.add(lineItems[0][:12])
cancerTypesFile.close()

exprFile = gzip.open(exprFilePath)

headerItems = exprFile.readline().rstrip("\n").split("\t")[1:]
sampleIndices = [i for i in range(len(headerItems)) if headerItems[i][:12] in sampleIDs]

outDict = {}
for line in exprFile:
    lineItems = line.rstrip("\n").split("\t")
    gene = lineItems[0]
    lineItems = lineItems[1:]

    outDict[gene] = {}

    for i in sampleIndices:
        sampleID = headerItems[i][:12]

        if sampleID not in outDict[gene]:
            outDict[gene][sampleID] = []

        outDict[gene][sampleID].append(lineItems[i])

exprFile.close()

# Average replicates and log-transform
for gene, sampleDict in outDict.items():
    for sampleID, values in sampleDict.items():
        outDict[gene][sampleID] = calcAverageAndLogTransform(values)

# Remove low-variance genes
genesToRemove = []
for gene, sampleDict in outDict.items():
    uniqueExpressionValues = set(sampleDict.values())
    if len(uniqueExpressionValues) <= 1:
        genesToRemove.append(gene)
for gene in genesToRemove:
    del outDict[gene]

genes = sorted(outDict.keys())

outFile = gzip.open(outFilePath, 'w')
outFile.write("\t{}\n".format("\t".join(genes)))

randomGene = outDict.keys()[0]
sampleIDs = sorted(outDict[randomGene].keys())
for sampleID in sampleIDs:
    outFile.write("{}\t{}\n".format(sampleID, "\t".join(["{:.6f}".format(outDict[gene][sampleID]) for gene in genes])))

outFile.close()
