import sys, gzip

inFilePath = sys.argv[1]
indices = [int(x) for x in sys.argv[2].split(",")]

unique = set()
numDuplicates = 0
with gzip.open(inFilePath) as inFile:
    inFile.readline()

    for line in inFile:
        line = line.decode().rstrip("\n")

#        if not "AUROC" in line:
#            continue

        lineItems = line.split("\t")
        #lineItems = tuple(lineItems[:6])
        #lineItems = tuple(lineItems[:5])
        #lineItems = (lineItems[0], lineItems[1], lineItems[2], lineItems[4])
        #lineItems = (lineItems[0], lineItems[1], lineItems[2], lineItems[4])
        lineItems = tuple([lineItems[i] for i in indices])

        if lineItems in unique:
            #print("Duplicate:")
            #print(lineItems)
            #sys.exit(0)
            numDuplicates += 1

        unique.add(lineItems)

for x in sorted(unique):
    print(x)
    break

print(len(unique))
print("Duplicates: {}".format(numDuplicates))
