import sys, gzip

inFilePath = sys.argv[1]
indices = [int(x) for x in sys.argv[2].split(",")]

uniqueDict = {}
with gzip.open(inFilePath) as inFile:
    inFile.readline()

    for line in inFile:
        line = line.decode().rstrip("\n")

        lineItems = line.split("\t")
        lineItems = tuple([lineItems[i] for i in indices])

        if lineItems in uniqueDict:
            uniqueDict[lineItems] += 1
        else:
            uniqueDict[lineItems] = 1

for key, value in sorted(uniqueDict.items()):
    if value < 1000:
        if key[0].startswith("sklearn"):
            print("rm -rfv Analysis4/*/*/*/tsv__" + key[0].replace("/", "__"))
        else:
            print("rm -rfv Analysis4/*/*/*/arff__" + key[0].replace("/", "__"))
