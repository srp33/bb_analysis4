import requests
import sys
import subprocess
import json
import glob
from random import randint

def getDirectoryContents(osf_id):
    if (len(osf_id) > 0):
      osf_id = osf_id + '/'
    url = 'https://files.osf.io/v1/resources/nrdsq/providers/osfstorage/' + osf_id
    response = requests.get(url,
      auth=('npgolightly@gmail.com','byu17piccolo')
    )
    json_data = json.loads(response.text)
    data = json_data['data']
    return data

def getOSFID(data,dir_name):
    for d in data:
      if (d['attributes']['name'] == dir_name):
        osf_id = d['attributes']['path'].split('/')[1]
        return osf_id

def createDirectory(base_dir_id, dir_name):
  print("Creating " + dir_name + " directory")
  if (len(base_dir_id) > 0):
    base_dir_id = base_dir_id + "/"

  url = 'https://files.osf.io/v1/resources/nrdsq/providers/osfstorage/' + base_dir_id + '?kind=folder'
  response = requests.put(url,
    auth=('npgolightly@gmail.com', 'byu17piccolo'),
    params={'kind': 'folder', 'name': dir_name}
  )
  json_data = json.loads(response.text)
  osf_id = json_data['data']['id'].split('osfstorage/')[1].strip('/')
  return osf_id

# IN PARAMETERS
analysis = sys.argv[1]
gseid = sys.argv[2]
class_name = sys.argv[3]
covariate_list = sys.argv[4]
iterations = sys.argv[5]

print('Analysis: ' + analysis)
print('gseid: ' + gseid)
print('class_name: ' + class_name)
print('covariate_list: ' + covariate_list)
print('iterations: ' + iterations)

data = getDirectoryContents('')

analysis_osf_id = getOSFID(data,analysis)
if analysis_osf_id == None:
  analysis_osf_id = createDirectory('',analysis)
analysis_data = getDirectoryContents(analysis_osf_id)

results_osf_id = getOSFID(analysis_data,'Results')
if results_osf_id == None:
  results_osf_id = createDirectory(analysis_osf_id,'Results')
results_data = getDirectoryContents(results_osf_id)

gseid_osf_id = getOSFID(results_data,gseid)
if gseid_osf_id == None:
  gseid_osf_id = createDirectory(results_osf_id,gseid)
gseid_data = getDirectoryContents(gseid_osf_id)

iterations_osf_id = getOSFID(gseid_data,'Iterations')
if iterations_osf_id == None:
  iterations_osf_id = createDirectory(gseid_osf_id,'Iterations')
iterations_data = getDirectoryContents(iterations_osf_id)

# Create a list of files existing on OSF
osf_files = list()
for d in iterations_data:
  osf_files.append(d['attributes']['name'])

for x in range(1,int(iterations)+1):
  file_name = class_name + '___iteration' + str(x) + '.zip'
  if (file_name in osf_files):
    print(file_name + ' has already been executed and is saved on OSF')
  else:
    if(covariate_list == 'no_covariates' and (analysis == 'Analysis2' or analysis == 'Analysis3')):
      print(file_name + ' has NO_COVARIATES, will not execute')
    else:
      if(analysis != 'Analysis4' and analysis != 'Analysis5'):
        print('Executing iteration ' + str(x) + ' of ' +  gseid + ' ' + class_name)
        subprocess.call('./execute ' + analysis + ' ' + gseid + ' ' + class_name + ' ' + covariate_list + ' 1 algo ' + str(x), shell=True)
      else:
        files = glob.glob("ClassificationAlgParams/*.list")
        for f in files:
          #print(f)
          extension = f.split('ClassificationAlgParams_')[1]
          extension = extension.split('.list')[0]
          file_name = class_name+ '___iteration' + str(x) + '___' + extension + '.zip'
#          print(file_name)
          if (file_name in osf_files):
            print(file_name + ' has already been executed and is saved on OSF') 
          else:
            print('Executing ... ' + file_name)
            input_file = open(f,'r')
            line = input_file.readline().strip('\n')
            algos = ''
            while line != '':
              if len(algos) > 0:
                algos = algos + ',' + line
              else:
                algos = line
              line = input_file.readline().strip('\n')
            input_file.close()
            if analysis == 'Analysis5':
             # subprocess.call('./execute5 ' + analysis + ' ' + gseid + ' ' + class_name + ' ' + covariate_list + ' ' + algos + ' ' + str(x) + ' ' + extension + ' /AlgorithmScripts/FeatureSelection/arff/weka/ReliefF/default' , shell=True)
             # subprocess.call('./execute5 ' + analysis + ' ' + gseid + ' ' + class_name + ' ' + covariate_list + ' ' + algos + ' ' + str(x) + ' ' + extension + ' /AlgorithmScripts/FeatureSelection/tsv/sklearn/svm_rfe/default' , shell=True)
              subprocess.call('./execute5 ' + analysis + ' ' + gseid + ' ' + class_name + ' ' + covariate_list + ' ' + algos + ' ' + str(x) + ' ' + extension + ' /AlgorithmScripts/FeatureSelection/tsv/sklearn/random_logistic_regression/default' , shell=True)
            else:
              subprocess.call('./execute4 ' + analysis + ' ' + gseid + ' ' + class_name + ' ' + covariate_list + ' ' + algos + ' ' + str(x) + ' ' + extension , shell=True)
