import sys
import glob

analysis = 'Analysis4'
input_dir = '/Analysis/BiomarkerBenchmark/' + analysis + '/'
output_dir = '/Analysis/BiomarkerBenchmark/Merged_Results/' + analysis + '/'

n_metrics = glob.glob(input_dir + '*/*/*/*/Nested_Metrics.tsv')
outfile = open(output_dir + 'Nested_Metrics.tsv', 'w')
outfile.write('GSE_ID\tDescription\tOuter_Iteration\tInner_Iteration\tAlgorithm\tMetric\tValue\n')

for m in n_metrics:
  inputfile = open(m, 'r')
  line =  inputfile.readline()
  line = inputfile.readline().strip()
  while line != '':
    cols = line.split('\t')
    o_gseid = cols[0].split('___')[0]
    o_description = cols[0].split('___')[1]
    o_outeriteration = cols[0].split('___iteration')[1].split('___Random')[0]
    o_inneriteration = cols[2]
    if ('AlgorithmScripts/' in cols[3]):
      o_algorithm = cols[3].split('AlgorithmScripts/Classification/')[1]
    else:
      o_algorithm = cols[3]
    o_metric = cols[4]
    o_value = cols[5]

    out = o_gseid + '\t' + o_description + '\t' + o_outeriteration + '\t' + o_inneriteration + '\t' + o_algorithm + '\t' + o_metric + '\t' + o_value + '\n'
    outfile.write(out)
    line = inputfile.readline().strip()

  inputfile.close()
outfile.close()

elapsed_times = glob.glob(input_dir + '*/*/*/*/Nested_ElapsedTime.tsv')
outfile = open(output_dir + 'Nested_ElapsedTime.tsv', 'w')
outfile.write('GSE_ID\tDescription\tOuter_Iteration\tInner_Iteration\tAlgorithm\tElapsedSeconds\n')

for e in elapsed_times:
  inputfile = open(e, 'r')
  line =  inputfile.readline()
  line = inputfile.readline().strip()
  while line != '':
    cols = line.split('\t')
    o_gseid = cols[0].split('___')[0]
    o_description = cols[0].split('___')[1]
    o_outeriteration = cols[0].split('___iteration')[1].split('___Random')[0]
    o_inneriteration = cols[2]
    if ('AlgorithmScripts/' in cols[3]):
      o_algorithm = cols[3].split('AlgorithmScripts/Classification/')[1]
    else:
      o_algorithm = cols[3]
    o_seconds = cols[4]

    out = o_gseid + '\t' + o_description + '\t' + o_outeriteration + '\t' + o_inneriteration + '\t' + o_algorithm + '\t' + o_seconds + '\n'
    outfile.write(out)
    line = inputfile.readline().strip()

  inputfile.close()
outfile.close()

print('Complete')
sys.exit()

n_predictions = glob.glob(input_dir + '*/*/*/*/Nested_Predictions.tsv')
outfile = open(output_dir + 'Nested_Predictions.tsv', 'w')
number_of_classes = ''
for p in n_predictions:
  inputfile = open(p,'r')
  headers = inputfile.readline().strip().split('\t')
  number_of_classes = len(headers)-6
  header = 'GSE_ID'
  counter = 0
  while counter < len(headers):
    header = header + '\t' + headers[counter]
    counter = int(counter + 1)
  outfile.write(header + '\n')
  break

for p in n_predictions:
  inputfile = open(p, 'r')
  line = inputfile.readline()
  line = inputfile.readline().strip()
  while line != '':
    cols = line.split('\t')
    o_gseid = cols[0].split('___')[0]
    o_description = cols[0].split('___')[1]
    o_outeriteration = cols[0].split('___iteration')[1].split('___Random')[0]
    o_inneriteration = cols[2]
    if ('AlgorithmScripts/' in cols[3]):
      o_algorithm = cols[3].split('AlgorithmScripts/Classification/')[1]
    else:
      o_algorithm = cols[3]
    o_instanceid = cols[4]
    o_actualclass = cols[5]
    o_predictedclass = cols[6]
    out = o_gseid + '\t' + o_description + '\t' + o_outeriteration + '\t' + o_inneriteration + '\t' + o_algorithm + '\t' + o_actualclass + '\t' + o_predictedclass
    counter = 0
    while counter < number_of_classes:
      out = out + '\t' + cols[6+counter]
      counter = int(counter + 1)
    out = out + '\n'
    outfile.write(out)
    line = inputfile.readline().strip()

  inputfile.close()
outfile.close()
