import os
import sys

analysis = sys.argv[1]

input = open('AllClassifications.txt','r')
line = input.readline()
gseid_class = set()

while line != '':
  line = line.strip('\n')
  gseid_class.add(line.split('\t')[0] + '___' + line.split('\t')[1] + '___' + line.split('\t')[2])
  line = input.readline()

for i in gseid_class:
  #print(analysis + ' ' + i.split('___')[0] + ' ' + i.split('___')[1] + ' ' + i.split('___')[2])
  if (analysis == 'Analysis1'):
    os.system('python executeIterations.py ' + analysis + ' '  + i.split('___')[0] + ' ' + i.split('___')[1] + ' no_covariates 10')
  else:
    #print('python executeIterations.py ' + analysis + ' '  + i.split('___')[0] + ' ' + i.split('___')[1] + ' ' + i.split('___')[2] + ' ' + '2')
    os.system('python executeIterations.py ' + analysis + ' '  + i.split('___')[0] + ' ' + i.split('___')[1] + ' ' + i.split('___')[2] + ' ' + '2')
