import glob
import sys
from random import randint

algos = glob.glob("AlgorithmScripts/Classification/*/*/*/*")
algos_remove = glob.glob("AlgorithmScripts/Classification/tsv/mlr/multinom/*")
algos_remove = algos_remove + glob.glob("AlgorithmScripts/Classification/arff/weka/MultilayerPerceptron/*")
algos_remove = algos_remove + glob.glob("AlgorithmScripts/Classification/tsv/mlr/boosting/*")
algos_remove = algos_remove + glob.glob("AlgorithmScripts/Classification/tsv/mlr/rda/*")

for a in algos_remove:
  algos.remove(a)

counter = 1
while(len(algos) > 0):
  algos_tmp = list()
  for y in range(10):
    if (len(algos) == 0):
      break
    ind = randint(0, len(algos)-1)
    algos_tmp.append(algos[ind])
    algos.remove(algos[ind])
    #print(algos_tmp)
    #print(len(algos))

  algo_all = ""
  for a in algos_tmp:
    if len(algo_all) > 0:
      algo_all = algo_all + ',' + a
    else:
      algo_all = a

  output_file = 'ClassificationAlgParams/ClassificationAlgParams_Random' + str(counter) + '.list'
  out = open(output_file,'w')
  for a in algos_tmp:
    out.write(a + '\n')
  out.close()
  counter = counter + 1
