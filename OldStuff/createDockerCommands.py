import sys
import os
import glob
import shutil

analysis = sys.argv[1]
iterations = sys.argv[2]
dirOutFilePath = sys.argv[3]
dockerOutFilePath = sys.argv[4]

currentWorkingDir = os.path.dirname(os.path.realpath(__file__))

dirsToCreate = []
dockerCommandFilePaths = []

allAlgorithmsPath = 'AllClassificationAlgorithms.list'
with open(allAlgorithmsPath, 'r') as f:
  allAlgorithms = f.read().splitlines()
allAlgorithms = set([x for x in allAlgorithms if not x.startswith("#")])

#allClassificationsPath = 'AllClassifications.txt'
allClassificationsPath = 'TestClassifications.txt'
with open(allClassificationsPath, 'r') as g:
  allClassifications = [x for x in g.read().splitlines() if not x.startswith("#")]

if (analysis == 'Analysis4'):
  if os.path.exists(analysis + '_Commands/'):
    shutil.rmtree(analysis + '_Commands/')
  for c in allClassifications:
    gseVar = c.split('\t')[0]
    classVar = c.split('\t')[1]
    covVar = c.split('\t')[2]

    input_data = list()
    dataset_path = 'Biomarker_Benchmark_Data/' + gseVar + '/'
    expression_path = dataset_path + gseVar + '.txt.gz'
    class_path = dataset_path + 'Class/' + classVar + '.txt'

    if covVar != 'no_covariates':
      input_data = covVar.split(',')
      input_data = [dataset_path + 'Covariate/' + i + '.txt' for i in input_data]

    input_data.append(expression_path)
    input_data.append(class_path)

    for i in range(1, 1+int(iterations)):
      print(analysis + ' ' + gseVar + ' ' + classVar + ' ' + 'iteration' + str(i))
      path = analysis + '/' + gseVar + '/' + classVar + '/iteration' + str(i) + '/*/Nested_Predictions.tsv'

      executed_algos = glob.glob(path)
      executed_algos = [x.split('/')[4].replace('__','/',3) for x in executed_algos]
      executed_algos = set(['AlgorithmScripts/Classification/' + x for x in executed_algos])

      not_executed_algos = allAlgorithms - executed_algos

      if len(not_executed_algos) > 0:
        for algo in not_executed_algos:
          data_all = ''
          for d in input_data:
            data_all = data_all + '--data "/InputData/' + d + '" '

          #tmpOutDir = currentWorkingDir + '/Temp/' + analysis + '/' + gseVar + '/' + classVar + '/iteration' + str(i) + '/' + algo.split('AlgorithmScripts/Classification/')[1].replace('/','__') + '/'
          outDir = '/Analysis/BiomarkerBenchmark/' + analysis + '/' + gseVar + '/' + classVar + '/iteration' + str(i) + '/' + algo.split('AlgorithmScripts/Classification/')[1].replace('/','__') + '/'

          out = 'if [ ! -f ' + outDir + 'Nested_Predictions.tsv ]\nthen\n  docker run --rm -v "/Analysis/BiomarkerBenchmark/":/InputData -v "' + outDir + '":/OutputData srp33/shinylearner:version348 "/UserScripts/nestedclassification_montecarlo" ' + data_all + '--description ' + gseVar + '___' + classVar + '___iteration' + str(i) + ' --outer-iterations 1 --inner-iterations 5 --classif-algo "' + algo + '" --output-dir "/OutputData" --seed ' + str(i) + '\n  mkdir -p ' + outDir + '\n  cp ' + outDir + 'Nested_ElapsedTime.tsv ' + outDir + 'Nested_ElapsedTime.tsv\n  cp ' + outDir + 'Nested_Metrics.tsv ' + outDir + 'Nested_Metrics.tsv\n  cp ' + outDir + 'Nested_Predictions.tsv ' + outDir + 'Nested_Predictions.tsv\nfi'

          commandFilePath = analysis + '_Commands/{}/{}/iteration{}/{}.sh'.format(gseVar, classVar, i, os.path.basename(algo))
          if not os.path.exists(os.path.dirname(commandFilePath)):
            os.makedirs(os.path.dirname(commandFilePath))

          with open(commandFilePath, 'w') as outFile:
            outFile.write(out + '\n')

          dirsToCreate.append(outDir)
          dockerCommandFilePaths.append(commandFilePath)

if len(dockerCommandFilePaths) == 0:
    print('All commands have been executed!')
else:
    with open(dirOutFilePath, 'w') as dirOutFile:
        for d in dirsToCreate:
            dirOutFile.write("mkdir -p {}\n".format(d))

    with open(dockerOutFilePath, 'w') as dockerOutFile:
        for command in dockerCommandFilePaths:
            dockerOutFile.write("bash {}\n".format(command))
