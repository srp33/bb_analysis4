library(readr)
library(dplyr)

inFilePath <- commandArgs()[7]
outFilePath <- commandArgs()[8]
startIteration <- as.integer(commandArgs())[9]
stopIteration <- as.integer(commandArgs())[10]

predData <- read_tsv(inFilePath)
algos <- sort(unique(predData$ClassificationAlgorithm))
corrMatrix <- NULL

#for (iteration in sort(unique(predData$Iteration)))
for (iteration in startIteration:stopIteration)
{
  print(paste0("Iteration: ", iteration))
  iterPredData <- NULL

  for (algo in algos)
  {
    smallPredData <- filter(predData, Iteration==iteration & ClassificationAlgorithm==algo) %>% select(-ClassificationAlgorithm, -Iteration)
    colnames(smallPredData)[2] <- algo

    if (is.null(iterPredData)) {
      iterPredData <- smallPredData
    } else {
      commonInstanceIDs <- intersect(iterPredData$InstanceID, smallPredData$InstanceID)

      if (length(commonInstanceIDs) != nrow(iterPredData)) {
        print("Inconsistent:")
        print(iteration)
        print(algo)
        print(length(commonInstanceIDs))
        print(nrow(iterPredData))
      }
      
      iterPredData <- inner_join(iterPredData, smallPredData, by="InstanceID")
    }
  }

  iterPredData <- select(iterPredData, -InstanceID) %>% as.matrix() %>% cor(method="spearman") %>% apply(1:2, function(x) { ifelse(is.na(x), 0, x) })

  if (is.null(corrMatrix)) {
    corrMatrix <- iterPredData
  } else {
    corrMatrix <- corrMatrix + iterPredData
  }
#  break
}

corrMatrix <- corrMatrix / length(algos)

# Get rid of algorithms where all predictions are identical (standard deviation is zero)
algosToKeep <- which(apply(corrMatrix, 2, function(x) { sum(x==0) }) <= 2)
algosToKeep <- names(algosToKeep)
corrMatrix <- corrMatrix[algosToKeep, algosToKeep]

rowNames <- rownames(corrMatrix)
corrMatrix <- as_tibble(corrMatrix)
corrMatrix <- cbind(rowNames, corrMatrix)
colnames(corrMatrix)[1] <- ""
print(corrMatrix[1:10,1:10])
write_tsv(corrMatrix, outFilePath)
