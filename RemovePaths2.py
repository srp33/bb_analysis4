import sys

inFilePath = sys.argv[1]

outLines = []

with open(inFilePath) as inFile:
    outLines.append(inFile.readline())

    for line in inFile:
        lineItems = line.rstrip("\n").split("\t")
        featureName = lineItems[-2]
        newFeatureName = featureName.split("__")[-1]
        lineItems[-2] = newFeatureName
        outLines.append("\t".join(lineItems) + "\n")

with open(inFilePath, 'w') as inFile:
    for line in outLines:
        inFile.write(line)
