import glob, gzip, os, shutil, sys

analysis = sys.argv[1]
startIteration = int(sys.argv[2])
stopIteration = int(sys.argv[3])
memoryGigs = sys.argv[4]
swapMemoryGigs = sys.argv[5]
hoursMax = sys.argv[6]
numCores = sys.argv[7]
fsAlgorithmsFilePath = sys.argv[8]
classifAlgorithmsFilePath = sys.argv[9]
classificationsFilePath = sys.argv[10]
outFileToCheck = sys.argv[11]
dockerOutFilePath = sys.argv[12]

def formatName(x, i):
    y = x.split("/")[i]
    z = y.split("__")
    a = "/".join(z[:4])
    if len(z) > 4:
        a += "__" + z[4]
    return a

currentWorkingDir = os.path.dirname(os.path.realpath(__file__))

dockerCommandFilePaths = []

with open(fsAlgorithmsFilePath, 'r') as f:
    fsAlgorithms = f.read().splitlines()
    fsAlgorithms = set([x for x in fsAlgorithms if not x.startswith("#")])

with open(classifAlgorithmsFilePath, 'r') as f:
    classifAlgorithms = f.read().splitlines()
    classifAlgorithms = set([x for x in classifAlgorithms if not x.startswith("#")])

algDelimiter = "________"
allAlgorithms = set()
for fsAlgorithm in fsAlgorithms:
    for classifAlgorithm in classifAlgorithms:
        fsAlgorithm = fsAlgorithm.replace('AlgorithmScripts/FeatureSelection/', "")
        classifAlgorithm = classifAlgorithm.replace('AlgorithmScripts/Classification/', "")
        algorithm = fsAlgorithm + algDelimiter + classifAlgorithm
        allAlgorithms.add(algorithm)

with open(classificationsFilePath, 'r') as g:
    allClassifications = [x for x in g.read().splitlines() if not x.startswith("#")]

if os.path.exists(analysis + '_Commands/'):
    shutil.rmtree(analysis + '_Commands/')

for c in allClassifications:
    gseVar = c.split('\t')[0]
    classVar = c.split('\t')[1]
    covVar = c.split('\t')[2]

    input_data = list()
    dataset_path = 'Biomarker_Benchmark_Data/' + gseVar + '/'
    expression_path = dataset_path + gseVar + '.txt.gz'
    class_path = dataset_path + 'Class/' + classVar + '.txt'

    if covVar != 'no_covariates':
        input_data = covVar.split(',')
        input_data = [dataset_path + 'Covariate/' + i + '.txt' for i in input_data]

    input_data.append(expression_path)
    input_data.append(class_path)

    data_all = ''
    for d in input_data:
        data_all = data_all + '--data "/InputData/' + d + '" '

    for i in range(startIteration, 1+stopIteration):
        print(analysis + ' ' + gseVar + ' ' + classVar + ' ' + 'iteration' + str(i))
        path = analysis + '/' + gseVar + '/' + classVar + '/iteration' + str(i) + '/*/*/' + outFileToCheck

        executed_algos = glob.glob(path)
        executed_algos = [formatName(x, 4) + algDelimiter + formatName(x, 5) for x in executed_algos]

        not_executed_algos = allAlgorithms - set(executed_algos)

        if len(not_executed_algos) > 0:
            for algo in not_executed_algos:
                fsAlgoName = algo.split(algDelimiter)[0]
                classifAlgoName = algo.split(algDelimiter)[1]

                fsAlgoPath = "AlgorithmScripts/FeatureSelection/" + fsAlgoName
                classifAlgoPath = "AlgorithmScripts/Classification/" + classifAlgoName

                fsAlgoName = "__".join(fsAlgoName.split("/")).replace("/", "__")
                classifAlgoName = "__".join(classifAlgoName.split("/")).replace("/", "__")

                outDir = '/Analysis/BiomarkerBenchmark/' + analysis + '/' + gseVar + '/' + classVar + '/iteration' + str(i) + '/' + fsAlgoName + '/' + classifAlgoName + "/"

                out = 'if [ ! -f ' + outDir + outFileToCheck + ' ]\nthen\n  docker run --memory ' + memoryGigs + 'G --memory-swap ' + swapMemoryGigs + 'G --rm -i -v "/Analysis/BiomarkerBenchmark/":/InputData -v "' + outDir + '":/OutputData srp33/shinylearner:version412 timeout -s 9 ' + hoursMax + 'h "/UserScripts/nestedboth_montecarlo" ' + data_all + '--description ' + gseVar + '___' + classVar + '___iteration' + str(i) + ' --outer-iterations 1 --inner-iterations 5 --fs-algo "' + fsAlgoPath + '" --classif-algo "' + classifAlgoPath + '" --num-features 1,10,100,1000,10000 --output-dir "/OutputData" --seed ' + str(i) + ' --verbose false --num-cores ' + numCores + '\n  docker run -v "' + outDir + '":/OutputData srp33/shinylearner:version407 /bin/bash -c "cd /OutputData; rm -f Log.txt; rm -f Archive.tar.gz; if [ -f Predictions.tsv ]; then tar -zcf Archive.tar.gz *.tsv; fi; rm -f *.tsv"\nfi'

                commandFilePath = analysis + '_Commands/{}/{}/iteration{}/{}.sh'.format(gseVar, classVar, i, fsAlgoName + algDelimiter + classifAlgoName)

                if not os.path.exists(os.path.dirname(commandFilePath)):
                    os.makedirs(os.path.dirname(commandFilePath))

                with open(commandFilePath, 'w') as outFile:
                    outFile.write(out + '\n')

                dockerCommandFilePaths.append(commandFilePath)

if len(dockerCommandFilePaths) == 0:
    print('All commands have been executed!')
else:
    with open(dockerOutFilePath, 'w') as dockerOutFile:
        for command in dockerCommandFilePaths:
            dockerOutFile.write("bash {}\n".format(command))
