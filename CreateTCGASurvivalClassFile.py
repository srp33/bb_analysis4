import sys, gzip

clinicalFilePath = sys.argv[1]
exprFilePath = sys.argv[2]
statusVariable = sys.argv[3]
daysVariable = sys.argv[4]
lastContactVariable = sys.argv[5]
lowThreshold = int(sys.argv[6])
highThreshold = int(sys.argv[7])
outFilePath = sys.argv[8]

exprFile = gzip.open(exprFilePath)
exprFile.readline()
exprSamples = set([line.rstrip("\n").split("\t")[0] for line in exprFile])
exprFile.close()

clinicalFile = gzip.open(clinicalFilePath)
clinHeaderItems = clinicalFile.readline().rstrip("\n").split("\t")

statusItems = None
daysItems = None
lastContactItems = None

for line in clinicalFile:
    lineItems = line.rstrip("\n").split("\t")
    variableName = lineItems[0]

    if variableName == statusVariable:
        statusItems = lineItems
    elif variableName == daysVariable:
        daysItems = lineItems
    elif variableName == lastContactVariable:
        lastContactItems = lineItems
    else:
        continue

if statusItems == None:
    print("Missing status variable.")
    sys.exit(1)
if daysItems == None:
    print("Missing days variable.")
    sys.exit(1)
if lastContactItems == None:
    print("Missing last contact variable.")
    sys.exit(1)

def parseClinical(clinHeaderItems, dataItems, description):
    clinicalDict = {}

    for i in range(3, len(clinHeaderItems)):
        sampleID = clinHeaderItems[i][:12]
        value = dataItems[i]

        if sampleID not in exprSamples:
            continue

        if sampleID in clinicalDict:
            if clinicalDict[sampleID] != value:
                print("There are multiple values for {} and {} in {}. This is problematic.".format(sampleID, variableName, clinicalFilePath))
                sys.exit(1)

        clinicalDict[sampleID] = value

    if len(clinicalDict) == 0:
        print("No matching clinical data were found for {}.".format(description))
        sys.exit(1)

    return clinicalDict

statusDict = parseClinical(clinHeaderItems, statusItems, statusVariable)
daysDict = parseClinical(clinHeaderItems, daysItems, daysVariable)
lastContactDict = parseClinical(clinHeaderItems, lastContactItems, lastContactVariable)

clinicalFile.close()

samplesToRemove = []
for sampleID, status in statusDict.items():
    days = daysDict[sampleID]
    lastContact = lastContactDict[sampleID]

    if status in set(["NA", "[Not Available]"]) or days in set(["NA"]) or lastContact in set(["NA"]):
        samplesToRemove.append(sampleID)

for sampleID in samplesToRemove:
    del statusDict[sampleID]
    del daysDict[sampleID]
    del lastContactDict[sampleID]

outFile = open(outFilePath, 'w')
outFile.write("SampleID\tClass\n")

numSTS = 0
numLTS = 0

for sampleID, status in statusDict.items():
    days = daysDict[sampleID]
    lastContact = lastContactDict[sampleID]

    if status == "Alive":
        if "[" in lastContact:
            continue
        if float(lastContact) < highThreshold:
            continue
        classValue = "LTS"
        numLTS += 1
    else:
        if "[" in days:
            continue
        if float(days) > lowThreshold and float(days < highThreshold):
            continue
        if float(days) <= lowThreshold:
            classValue = "STS"
            numSTS += 1
        else:
            classValue = "LTS"
            numLTS += 1

    outFile.write("{}\t{}\n".format(sampleID, classValue))

outFile.close()

print("STS: {}".format(numSTS))
print("LTS: {}".format(numLTS))
