import sys

inFilePath = sys.argv[1]

outLines = []

with open(inFilePath) as inFile:
    outLines.append(inFile.readline())

    for line in inFile:
        lineItems = line.rstrip("\n").split("\t")
        featureNames = lineItems[-1].split(",")
        newFeatureNames = [x.split("__")[-1] for x in featureNames]
        lineItems[-1] = ",".join(newFeatureNames)
        outLines.append("\t".join(lineItems) + "\n")

with open(inFilePath, 'w') as inFile:
    for line in outLines:
        inFile.write(line)
