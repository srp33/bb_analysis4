import sys, gzip

clinicalFilePath = sys.argv[1]
exprFilePath = sys.argv[2]
variableToQuery = sys.argv[3]
valuesToKeep = sys.argv[4].split("|")
valuesToExclude = sys.argv[5].split("|")
replacePatterns = sys.argv[6].split("|")
outFilePath = sys.argv[7]

def replaceValue(value):
    if len(replacePatterns) == 1 and replacePatterns[0] == "":
        return value

    for replacePattern in replacePatterns:
        search = replacePattern.split("=")[0]
        replace = replacePattern.split("=")[1]

        if value == search:
            value = replace
            break

    return value

valuesToExclude.extend(["[Not Available]", "[Unknown]", "NA"])

exprFile = gzip.open(exprFilePath)
exprFile.readline()
exprSamples = set([line.rstrip("\n").split("\t")[0] for line in exprFile])
exprFile.close()

clinicalDict = {}
clinicalFile = gzip.open(clinicalFilePath)
clinHeaderItems = clinicalFile.readline().rstrip("\n").split("\t")

for line in clinicalFile:
    lineItems = line.rstrip("\n").split("\t")
    variableName = lineItems[0]

    if variableName != variableToQuery:
        continue

    for i in range(3, len(clinHeaderItems)):
        sampleID = clinHeaderItems[i][:12]
        value = lineItems[i]

        if sampleID not in exprSamples:
            continue
        if valuesToKeep[0] != "keepall" and value not in valuesToKeep:
            continue
        if value in valuesToExclude:
            continue

        if sampleID in clinicalDict:
            if clinicalDict[sampleID] != value:
                print("There are multiple values for {} and {} in {}. This is problematic.".format(sampleID, variableName, clinicalFilePath))
                sys.exit(1)

        clinicalDict[sampleID] = value

clinicalFile.close()

if len(clinicalDict) == 0:
    print("No matching clinical data were found.")
    sys.exit(1)
if valuesToKeep[0] != "keepall" and len(set(clinicalDict.values())) != len(valuesToKeep):
    print("The number of matching values is inequal to the number specified.")
    print(set(clinicalDict.values()))
    sys.exit(1)

outFile = open(outFilePath, 'w')
outFile.write("SampleID\t{}\n".format(variableToQuery.replace(" ", "_").replace("-", "_").replace(">", "gt")))
for sampleID, value in clinicalDict.items():
    value = replaceValue(value)
    outFile.write("{}\t{}\n".format(sampleID, value.replace(" ", "_").replace("-", "_").replace(">", "gt")))
outFile.close()
