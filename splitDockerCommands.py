import os, sys, glob

commandsFilePath = sys.argv[1]
numPer = int(sys.argv[2])
outDirPath = sys.argv[3]

with open(commandsFilePath) as commandsFile:
    commands = [line.rstrip("\n") for line in commandsFile]

count = 1
while len(commands) > 0:
    outFilePath = "{}/{}".format(outDirPath, count)
    count += 1

    if len(commands) <= numPer:
        iterCommands = commands
        commands = []
    else:
        iterCommands = commands[:numPer]
        commands = commands[numPer:]

    with open(outFilePath, 'w') as outFile:
        outFile.write("\n".join(iterCommands) + "\n")
