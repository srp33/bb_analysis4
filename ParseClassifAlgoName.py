import sys, os

colIndex = int(sys.argv[1])

print(sys.stdin.readline().rstrip("\n"))

for line in sys.stdin:
    lineItems = line.rstrip("\n").split("\t")
    algorithm = lineItems[colIndex]

    algorithm = os.path.basename(os.path.dirname(algorithm))
    algItems = algorithm.split("__")
    algorithm = "{}/{}/{}".format(algItems[0], algItems[1], "__".join(algItems[2:]))

    lineItems[colIndex] = algorithm
    print("\t".join(lineItems))
