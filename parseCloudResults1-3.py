import os
import sys
import glob
import shutil
import subprocess

analysis = sys.argv[1]

output_dir = analysis + '_Results/'
if not os.path.exists(output_dir):
  os.makedirs(output_dir)

def mergeAll(filename):
  in_path = temp_dir + filename
  if not os.path.exists(in_path):
    return False
  with open(in_path) as p:
    data = p.readlines()
  data_header = data[0]
  data.pop(0)
  data = [d.strip().split('\t') for d in data]
  for line in data:
    gse_id = line[0].split('___')[0]
    class_id = line[0].split('___')[1]
    iteration_id = line[0].split('___')[2]
    if 'Ensemble' in line[2]:
      continue
    algorithm_id = line[2].split('AlgorithmScripts/Classification/')[1]
    algorithm_id = algorithm_id.replace('/','__')
    gse_id_dir = output_dir + gse_id + '/'

    if not os.path.exists(gse_id_dir):
      os.makedirs(gse_id_dir)
    class_id_dir = gse_id_dir + class_id + '/'
    if not os.path.exists(class_id_dir):
      os.makedirs(class_id_dir)
    iteration_id_dir = class_id_dir + iteration_id + '/'
    if not os.path.exists(iteration_id_dir):
      os.makedirs(iteration_id_dir)
    algorithm_id_dir = iteration_id_dir + algorithm_id + '/'
    if not os.path.exists(algorithm_id_dir):
      os.makedirs(algorithm_id_dir)

    outline = ''
    for i in range(len(line)):
        outline = outline + '\t' + line[i]
    outline = outline + '\n'
    data_outpath = algorithm_id_dir + filename

    if not os.path.exists(data_outpath):
      with open(data_outpath, 'w') as outfile:
        outfile.write(data_header)
    else:
      with open(data_outpath, 'a') as outfile:
        outfile.write(outline)
  return True
  
filepaths = glob.glob(analysis + '/GSE*/Iterations/*.zip')
for fp in filepaths:
  print(fp)
  filename = fp.split('Iterations/')[1]
  temp_dir = "temp/"
  if not os.path.exists(temp_dir):
    os.makedirs(temp_dir)
  subprocess.call('cp ' + fp + ' ' + temp_dir, shell=True)
  subprocess.call('unzip ' + temp_dir + filename + ' -d ' + temp_dir + ' > /dev/null' , shell=True)
  
  metrics_name = 'Metrics.tsv'
  predictions_name = 'Predictions.tsv'
  elapsed_time_name = 'ElapsedTime.tsv'

  if(mergeAll(metrics_name) and mergeAll(predictions_name) and mergeAll(elapsed_time_name)):
    shutil.rmtree(temp_dir)
    continue
  else:
    #subprocess.call('rm ' + fp,shell=True)
    shutil.rmtree(temp_dir)
  break
