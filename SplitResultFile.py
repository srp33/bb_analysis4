import os, sys

inFilePath = sys.argv[1]
outBaseDirPath = sys.argv[2]

with open(inFilePath) as inFile:
    headerLine = inFile.readline()

    otherLines = [line.rstrip().split("\t") for line in inFile]
    uniqueAlgos = set([x[2] for x in otherLines])

    for algo in uniqueAlgos:
        algoLines = [line for line in otherLines if line[2] == algo]
        shortAlgo = algo.replace("/AlgorithmScripts/Classification/", "").replace("/", "__")
        outDirPath = outBaseDirPath + "/" + shortAlgo
        outFilePath = outDirPath + "/" + os.path.basename(inFilePath)

        os.makedirs(outDirPath, exist_ok=True)

        print(outFilePath)
        with open(outFilePath, 'w') as outFile:
            outFile.write(headerLine)
            for line in algoLines:
                outFile.write("\t".join(line) + "\n")
